package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // Implemented solution here
        if (inputNumbers.contains(null) || !isTriangle(inputNumbers.size())) throw new CannotBuildPyramidException();
        int height = calcHeight(inputNumbers.size());
        int width = height*2-1;
        /**
         * Sorting input data
         */
        Collections.sort(inputNumbers);
        /**
         * Fill the matrix by 0
         */
        int[][] result = new int[height][width];
        for (int[] hght: result) {
            Arrays.fill(hght, 0);
        }
        /**
         * Fill the matrix by input data
         */
        int listPosition = inputNumbers.size()-1;
        for (int i = height-1, offset = 0; i >= 0; i--, offset++){
            for (int j = width-1-offset; j >= offset; j -= 2) {
                result[i][j] = inputNumbers.get(listPosition);
                listPosition--;
            }
        }
        
        return result;
    }
    /**
     * Checking if pyramid can be build from input data
     */    
    public static boolean isTriangle(long num) {
        long find_num = 8*num+1;
        long sqrt_num =(long) Math.sqrt(find_num);
        if (sqrt_num * sqrt_num == find_num ) {
            return true;
        }
        return false;
    }
    /**
     * Calculating Y dimention of matrix
     * @param num
     * @return height
     */
    public static int calcHeight(int num) {
        return (int)(-1+Math.sqrt(1+8*num))/2;
        }


}
