package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // Implemented the logic here
        String result = null;
        try {
        result = Double.toString(count(preparePostfixExpression(statement))).replace(".0", "");
        } catch (Exception e) {
            
        }
        return result;
    }
    
    private static boolean isDelimiter(char ch) {
        return ch == ' ';
    }
    
    private static boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(' || ch == ')';
    }
    
    private static boolean isDot(char ch) {
        return ch == '.';
    }
    
    private static byte getPriority(char ch) {
        switch (ch){
            case '(' : return 0;
            case ')' : return 1;
            case '+' : return 2;
            case '-' : return 2;
            case '*' : return 3;
            case '/' : return 3;
            default: return 4;            
        }
    }
    
    private static boolean isDouble(String s) {
    if( s == null ) return false;
    try {
        Double.parseDouble(s);
        return true;
    } catch (NumberFormatException e) {
        return false;
    }
    }
    
    private static String preparePostfixExpression (String input) {
        StringBuilder builder = new StringBuilder();
        Stack<Character> operators = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            if (isDelimiter(input.charAt(i))) continue;
            if (Character.isDigit(input.charAt(i)) || isDot(input.charAt(i))) {
                while (!isDelimiter(input.charAt(i)) && !isOperator(input.charAt(i))) {
                    builder.append(Character.toString(input.charAt(i)));
                    i++;
                    if (i == input.length()) break;
                }
                builder.append(" ");
                i--;
            }
            if (isOperator(input.charAt(i))) {
                if (input.charAt(i) == '(') operators.push(input.charAt(i));
                else if (input.charAt(i) == ')') {
                    char s = operators.pop();
                    while (s != '(') {
                        builder.append(Character.toString(s));
                        try {
                        s = operators.pop();
                        } catch (EmptyStackException e) {
                            System.err.println("Incorrect input data format!");
                            break;
                        }
                    }
                } else {
                    if (!operators.isEmpty()) {
                        if (getPriority(input.charAt(i)) <= getPriority(operators.peek())) {
                            builder.append(Character.toString(operators.pop())).append(" ");
                        }                        
                    }
                    operators.push(input.charAt(i));                    
                }
            }         
        }
        while (!operators.isEmpty()) {
                builder.append(Character.toString(operators.pop())).append(" ");
            }
        return builder.toString();
    }
    
    private static double count(String input) {
        double result = 0;
        Stack<Double> tmp = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i)) || isDot(input.charAt(i))) {
                StringBuilder builder = new StringBuilder();
                while (!isDelimiter(input.charAt(i)) && !isOperator(input.charAt(i))) {
                    builder.append(Character.toString(input.charAt(i)));
                    i++;
                    if (i == input.length()) break;
                }
                if (isDouble(builder.toString())) {
                    tmp.push(Double.valueOf(builder.toString()));
                    i--;
                } else throw new IllegalArgumentException("Wrong input data format!");
            } else if (isOperator(input.charAt(i))) {
                double a = tmp.pop();
                double b = tmp.pop();
                
                switch (input.charAt(i)) {
                    case '+' : result = b + a;
                    break;
                    case '-' : result = b - a;
                    break;
                    case '*' : result = b * a;
                    break;
                    case '/' : if (a == 0) throw new ArithmeticException();
                    else result = b / a;
                }
                tmp.push(result);
            }
             
        }
        return tmp.peek();
    }

}
